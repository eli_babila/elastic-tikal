

Elasticsearch For Developers

1. Introduction to Elasticsearch and the ELK stack
    1.1 What is Elasticsearch?
    1.2 History
    1.3 Basic concepts
2. Installing Elasticsearch
    2.1 Installing locally (docker)
    2.2 Production possibilities
3. Document management
    3.1 Create and delete indices
    3.1 CRUD documents 
4. Mapping
    4.1 Dynamic mapping and Explicit mapping
    4.2 Possible mappings
5. Searching
    5.1 Using the request URI
    5.2 Elastic query DSL
    5.3 Query context and filter context
    5.4 Seatch results overview
6. Aggregations
    6.1 Intro to aggregations
    6.2 Bucket aggregations
    6.3 Metric aggregations
    6.4 Histograms
    6.5 Other aggergations
7. Relations with documents
    7.1 Nested objects
    7.2 Parent child relashionship
8. Conclusion


