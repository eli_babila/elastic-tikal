# Elastic Tutorial #

This repository contains an Elastic tutorial, split into chapters.

### How To Get Started ###

You'll need a *shell* with *curl* to send commands to Elastic. Chapter 01 explains how to install Elastic using *Docker*, so Docker is needed although other install methods can be used as well.

### Table Of Contents ###

* 01 - Installing
* 02 - Indexing
* 03 - Basic Searching
* 
* 
* 

### How To ###

* Writing tests
* Code review
* Other guidelines

