# Installing #

There are several way to install Elastic. For personal use, the easiest cross platform way is using Docker.



### Intalling using Docker ###

Pull the docker image using

```
docker pull docker.elastic.co/elasticsearch/elasticsearch:7.6.1
```


Now a single node cluster can be started with 

```
docker run -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" -v $(pwd)/esdata/:/usr/share/elasticsearch/data  docker.elastic.co/elasticsearch/elasticsearch:7.6.1

```

### Verifying the installation ###

Verify the installation is ok by issuing a curl GET locally to port 9200.

```
curl localhost:9200
```

A response should be similar to

```
{
  "name" : "5d49ae1698e6",
  "cluster_name" : "docker-cluster",
  "cluster_uuid" : "L-pJl8P2Qx2C-IGyx0_JrA",
  "version" : {
    "number" : "7.6.1",
    "build_flavor" : "default",
    "build_type" : "docker",
    "build_hash" : "aa751e09be0a5072e8570670309b1f12348f023b",
    "build_date" : "2020-02-29T00:15:25.529771Z",
    "build_snapshot" : false,
    "lucene_version" : "8.4.0",
    "minimum_wire_compatibility_version" : "6.8.0",
    "minimum_index_compatibility_version" : "6.0.0-beta1"
  },
  "tagline" : "You Know, for Search"
}
```


### Other installation options ###

See [here](https://www.elastic.co/guide/en/elasticsearch/reference/current/install-elasticsearch.html) in case Docker is not available or you prefer to try another way to install Elastic.
